#include<iostream>
#include <eigen3/Eigen/Dense>
#include<fstream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
using namespace std;
using namespace Eigen;
using namespace cv;

int HEI,WID;
MatrixXf fx1,fy1,fx2,fy2,fx3, fy3;
Matrix3d fm;
MatrixXf mask;
MatrixXi img1, img2;

MatrixXf ReadMat(const char* filename, int height, int width)
{
    ifstream myfile(filename);
    MatrixXf mat(height, width);

    for(int h=0;h<height;h++)
    {
        for(int w=0;w<width;w++)
        {
            float tmp;
            myfile>>tmp;
            mat(h,w)=tmp;
        }
    }
    myfile.close();
    return mat;
}

void ReadFundamentalMatrix()
{
    std::ifstream fundamentalFileStream("frame_0001_fund.dat", std::ios_base::in | std::ios_base::binary);
    double elements[9];
    int directionFlag;
    fundamentalFileStream.read(reinterpret_cast<char*>(elements), 9*sizeof(double));
    fundamentalFileStream.read(reinterpret_cast<char*>(&directionFlag), sizeof(int));
    fundamentalFileStream.close();

    fm << elements[0], elements[1], elements[2],
                   elements[3], elements[4], elements[5],
                   elements[6], elements[7], elements[8];
  //  cout<<fm<<endl;
}

void ReadImages()
{
    cv::Mat image1=imread("sintel_img/frame_0001.png",0), image2=imread("sintel_img/frame_0002.png",0);
    MatrixXi img1t(HEI,WID), img2t(HEI,WID);
    for(int h=0;h<HEI;h++)
    {
        for(int w=0;w<WID;w++)
        {
            img1t(h,w)=image1.at<uchar>(h,w);
            img2t(h,w)=image2.at<uchar>(h,w);
        }
    }
    img1=img1t;
    img2=img2t;
}

void ReadData()
{
    HEI=436;
    WID=1024;
    fx1=ReadMat("sintel_sgm/fx.txt",HEI,WID);
    fy1=ReadMat("sintel_sgm/fy.txt",HEI,WID);
    fx2=ReadMat("sintel_gt/fx.txt",HEI,WID);
    fy2=ReadMat("sintel_gt/fy.txt",HEI,WID);
    mask=ReadMat("mask_out.txt",HEI,WID);
    ReadFundamentalMatrix();
    ReadImages();
}

void GetAPE()
{
    double tolE=0;
    int cnt=0;
    for(int h=0;h<HEI;h++)
    {
        for(int w=0;w<WID;w++)
        {
            if(mask(h,w)<125)
                continue;
            tolE+=sqrt(pow(fx1(h,w)-fx2(h,w),2)+pow(fy1(h,w)-fy2(h,w),2));
            cnt++;
        }
    }
    cout<<tolE/cnt<<endl;
}

void computeSecondEpipolarLine(const double firstX, const double firstY, Eigen::Vector3d& secondEpipolarLine)
{
	Eigen::Vector3d firstPoint;
	firstPoint << firstX, firstY, 1.0;

	secondEpipolarLine = fm*firstPoint;
	double normalizeFactor = sqrt(secondEpipolarLine(0)*secondEpipolarLine(0) + secondEpipolarLine(1)*secondEpipolarLine(1));
	if (normalizeFactor > 1e-6) normalizeFactor = 1.0/normalizeFactor;
	else normalizeFactor = 1.0;
	secondEpipolarLine *= normalizeFactor;
}


MatrixXf EvaluateEpipolar()
{
    //cout<<hm<<endl;
    MatrixXf res(HEI,WID);
    double totalDist=0;
    for(int h=0;h<HEI;h++)
    {
        for(int w=0;w<WID;w++)
        {
            //assume no rotation
            int x=w, y=h;
            Vector3d epipolarLine;
            computeSecondEpipolarLine(x,y,epipolarLine);

			double secondX=x+fx1(h,w), secondY=y+fy1(h,w);
			double a=epipolarLine(0), b=epipolarLine(1), c=epipolarLine(2);
			double dist=fabs(a*secondX+b*secondY+c)/sqrt(a*a+b*b);
            totalDist+=dist;
            res(h,w)=dist;
        }
    }
    cout<<totalDist/(HEI*WID)<<endl;
    return res;
}

void ComputeProjectionOnEpipolarLine(double x0, double y0, double a, double b, double c, double& x1, double& y1)
{
    //x1=(-a*b*centerY+b*b*centerX-a)/(a*a-b*b);
    //y1=(a*a*centerY-a*b*centerX+b)/(a*a-b*b);
    x1=(-a*b*y0+b*b*x0-a*c)/(a*a+b*b);
    y1=(a*a*y0-a*b*x0-b*c)/(a*a+b*b);

}

double GetPixelVal(MatrixXi img, double x, double y)
{
    int floorX=floor(x), floorY=floor(y), ceilX=ceil(x), ceilY=ceil(y);
    if(ceilX==floorX)
        ceilX++;
    if(ceilY==floorY)
        ceilY++;

    float fx=x-floorX, fy=y-floorY, fx1=1-fx, fy1=1-fy;
    int c1=img(floorY,floorX), c2=img(floorY,ceilX), c3=img(ceilY,floorX), c4=img(ceilY,ceilX);

    double c=c1*fx1*fy1+c2*fx*fy1+fx1*fy*c3+fx*fy*c4;
    return c;
}

double ComparePatch(double x1, double y1, double x2, double y2)
{
    double res=0;
    for(int y=-2;y<=2;y++)
    {
        for(int x=-2;x<=2;x++)
        {
            double x1c=x1+x,y1c=y1+y,x2c=x2+x,y2c=y2+y;
            if(x1c<0 || x1c>=WID-1 || y1c<0 || y1c>=HEI-1 ||x2c<0 || x2c>=WID-1 || y2c<0 ||y2c>=HEI-1)
                continue;
            double val1=GetPixelVal(img1,x1c,y1c), val2=GetPixelVal(img2,x2c,y2c);
            res+=(val1-val2)*(val1-val2);
        }
    }
    return res;
}

double SearchAlong(double x0, double y0, double x, double y, double u, double v)
{
    double minDist=FLT_MAX;
    double minI;
    for(double i=-0.1;i<=0.1;i+=0.01)
    {
        double newX=x+i*u, newY=y+i*v;
        double curDist=ComparePatch(x0,y0,newX,newY);
        if(curDist<minDist)
        {
            minDist=curDist;
            minI=i;
        }
    }
    return minI;
}

void RefineFlow()
{
    fx3=MatrixXf(HEI,WID);
    fy3=MatrixXf(HEI,WID);
    double totErr1=0, totErr2=0;
    for(int h=0;h<HEI;h+=20)
    {
        for(int w=0;w<WID;w+=20)
        {
            if(mask(h,w)<=125)
            {
                fx3(h,w)=fx1(h,w);
                fy3(h,w)=fy1(h,w);
                continue;
            }
            int x=w, y=h;
            double secondX=x+fx1(h,w), secondY=y+fy1(h,w);
            Vector3d epipolarLine;
            computeSecondEpipolarLine(x,y,epipolarLine);
            double a=epipolarLine(0), b=epipolarLine(1), c=epipolarLine(2);

            double x1, y1;
            ComputeProjectionOnEpipolarLine(secondX,secondY,a,b,c,x1,y1);

            cout<<secondX<<" "<<secondY<<" "<<x1<<" "<<y1<<" "<<x+fx2(h,w)<<" "<<y+fy2(h,w)<<endl;
            /*double i=SearchAlong(x,y,x1,y1,epipolarLine(2),-epipolarLine(1));
            fx3(h,w)=x1+epipolarLine(2)*i-x;
            fy3(h,w)=y1-epipolarLine(1)*i-y;
            //cout<<secondX<<" "<<secondY<<" "<<x1<<" "<<y1<<" "<<x1+epipolarLine(2)*i
           // <<" "<<y1-epipolarLine(1)*i<<" "<<x+fx2(h,w)<<" "<<y+fy2(h,w)<<endl;
            double err1=(fx1(h,w)-fx2(h,w))*(fx1(h,w)-fx2(h,w))
            +(fy1(h,w)-fy2(h,w))*(fy1(h,w)-fy2(h,w)),
            err2=(fx3(h,w)-fx2(h,w))*(fx3(h,w)-fx2(h,w))
            +(fy3(h,w)-fy2(h,w))*(fy3(h,w)-fy2(h,w));
            totErr1+=err1;
            totErr2+=err2;*/
        }
    }
    cout<<totErr1<<" "<<totErr2<<endl;
}

void SaveRes(MatrixXf res)
{
    float curMax=0;
    for(int h=0;h<HEI;h++)
    {
        for(int w=0;w<WID;w++)
        {
            if(res(h,w)>curMax)
            {
                curMax=res(h,w);
            }
        }
    }
    Mat myMat(HEI, WID, CV_8UC1);
    for(int h=0;h<HEI;h++)
    {
        for(int w=0;w<WID;w++)
        {
            myMat.at<uchar>(h,w)=255*pow(res(h,w)/curMax,0.5);
        }
    }
    imwrite("depth_out.png", myMat);
}

void EvaluateEpipolar2()
{
    //cout<<hm<<endl;
    double dist1=0,dist2=0;
    for(int h=0;h<HEI;h++)
    {
        for(int w=0;w<WID;w++)
        {
            if(mask(h,w)<=125)
            {

                continue;
            }
            //assume no rotation
            int x=w, y=h;
            Vector3d epipolarLine;
            computeSecondEpipolarLine(x,y,epipolarLine);

			double secondX=x+fx1(h,w), secondY=y+fy1(h,w),
			secondX2=x+fx2(h,w),secondY2=y+fy2(h,w);
			double a=epipolarLine(0), b=epipolarLine(1), c=epipolarLine(2);
			dist1+=fabs(a*secondX+b*secondY+c)/sqrt(a*a+b*b);
			dist2+=fabs(a*secondX2+b*secondY2+c)/sqrt(a*a+b*b);
        }
    }
    cout<<dist1<<" "<<dist2<<endl;
}


int  main()
{
   // double x,y;
   // ComputeProjectionOnEpipolarLine(0,1,-2,1,0,x,y);
   // cout<<x<<" "<<y<<endl;
    ReadData();
    //RefineFlow();
    GetAPE();
    //EvaluateEpipolar();
   // MatrixXf res=EvaluateEpipolar();
    //SaveRes(res);
}

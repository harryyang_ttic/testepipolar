#include<iostream>
#include <eigen3/Eigen/Dense>
#include <fstream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
//#include "FundamentalMatrix.h"

using namespace std;
using namespace Eigen;
using namespace cv;

int height, width;

Matrix3d fm;
MatrixXf fx, fy, mask;
Matrix3d homographyMatrix;

MatrixXf img1,img2;

char* fx_name, *fy_name, *fm_name, *hm_name, *out_name, *mask_name;

void ParseArguments(int argc, char** argv)
{
    if(argc < 6)
    {
        cout<<"fx_name, fy_name, fm_name, height, width"<<endl;
        getchar();
        exit(-1);
    }
    fx_name=argv[1];
    fy_name=argv[2];
    fm_name=argv[3];
   // mask_name=argv[4];
    height=atoi(argv[4]);
    width=atoi(argv[5]);
}

MatrixXf ReadMat(const char* filename, int height, int width)
{
    ifstream myfile(filename);
    MatrixXf mat(height, width);

    for(int h=0;h<height;h++)
    {
        for(int w=0;w<width;w++)
        {
            float tmp;
            myfile>>tmp;
            mat(h,w)=tmp;
        }
    }
    myfile.close();
    return mat;
}

void ReadFlow()
{
    fx=ReadMat(fx_name, height, width);
    fy=ReadMat(fy_name, height, width);
}

void ReadFundamentalMatrix()
{
    std::ifstream fundamentalFileStream(fm_name, std::ios_base::in | std::ios_base::binary);
    double elements[9];
    int directionFlag;
    fundamentalFileStream.read(reinterpret_cast<char*>(elements), 9*sizeof(double));
    fundamentalFileStream.read(reinterpret_cast<char*>(&directionFlag), sizeof(int));
    fundamentalFileStream.close();

    fm << elements[0], elements[1], elements[2],
                   elements[3], elements[4], elements[5],
                   elements[6], elements[7], elements[8];
}


void ReadData()
{
    ReadFlow();
    mask=ReadMat(mask_name,height,width);
    ReadFundamentalMatrix();
}

void computeSecondEpipolarLine(const double firstX, const double firstY, Eigen::Vector3d& secondEpipolarLine)
{
	Eigen::Vector3d firstPoint;
	firstPoint << firstX, firstY, 1.0;

	secondEpipolarLine = fm*firstPoint;
	double normalizeFactor = sqrt(secondEpipolarLine(0)*secondEpipolarLine(0) + secondEpipolarLine(1)*secondEpipolarLine(1));
	if (normalizeFactor > 1e-6) normalizeFactor = 1.0/normalizeFactor;
	else normalizeFactor = 1.0;
	secondEpipolarLine *= normalizeFactor;
}

/*void TestConstraint()
{
    for(int h=0;h<height;h++)
    {
        for(int w=0;w<width;w++)
        {
            int x=w, y=h;
            ComputePin3D();
            ComputePinCamera2();
            ComputeEpipolarLine2();
            ComputeIntersection1();
            ComputeIntersection2();
        }
    }
}*/

void EvaluateEpipolar(Matrix3d hm)
{
    double res=0;
    int cnt=0;
    //cout<<hm<<endl;
    for(int h=0;h<height;h++)
    {
        for(int w=0;w<width;w++)
        {
            //assume no rotation
            if(mask(h,w)==1)
                continue;
            int x=w, y=h;
            Vector3d epipolarLine;
            computeSecondEpipolarLine(x,y,epipolarLine);

            Vector3d imagePoint;
            imagePoint << x,y,1;
            Vector3d rotatedPoint = hm * imagePoint;
            rotatedPoint /= rotatedPoint(2);

            double coefficientToEpipolarLine = -epipolarLine(0)*rotatedPoint(0)
			- epipolarLine(1)*rotatedPoint(1) - epipolarLine(2);
            double noDisparityPointX = rotatedPoint(0) + epipolarLine(0)*coefficientToEpipolarLine;
			double noDisparityPointY = rotatedPoint(1) + epipolarLine(1)*coefficientToEpipolarLine;

            double epipolarDirectionX = -epipolarLine(1);
			double epipolarDirectionY = epipolarLine(0);
            /*if(x==100 && y==100)
            {
                cout<<x<<" "<<y<<" "<<rotatedPoint(0)<<" "<<rotatedPoint(1)<<" "<<coefficientToEpipolarLine
			<<" "<<noDisparityPointX<<" "<<noDisparityPointY<<endl;
			cout<<epipolarLine(0)<<" "<<epipolarLine(1)<<" "<<epipolarLine(2)<<endl;
            }*/


			double secondX=x+fx(h,w), secondY=y+fy(h,w);
			double a=-epipolarDirectionY/(noDisparityPointX*epipolarDirectionY-noDisparityPointY*epipolarDirectionX),
			b=epipolarDirectionX/(noDisparityPointX*epipolarDirectionY-noDisparityPointY*epipolarDirectionX),
			c=1;

			double dist=fabs(a*secondX+b*secondY+c)/sqrt(a*a+b*b);
			cnt++;
			//res+=fabs(diffX/epipolarDirectionX-diffY/epipolarDirectionY);
			res+=dist;
        }
    }
    //cout<<cnt<<" "<<res<<endl;
}

Matrix3d GetRandomCalibMatrix()
{
    Matrix3d calib;
    /*calib<<999.7838133341337,0, 792.9412307739258,
    0, 999.7838133341337,609.6846313476562,
    0,0, 1;*/
    calib<<707.09, 0, 601.89,0,707.09,183.11,0,0,1;
    return calib;
}



Matrix3d ComputeHomographyMatrix(Matrix3d calibrationMatrix_, Matrix3d fundamentalMatrix_)
{
	Eigen::Matrix3d essentialMatrix = calibrationMatrix_.transpose()*fundamentalMatrix_*calibrationMatrix_;
	Eigen::JacobiSVD<Eigen::Matrix3d> svdEssential(essentialMatrix, Eigen::ComputeFullU | Eigen::ComputeFullV);
	Eigen::Matrix3d matW;
	matW << 0, -1, 0,
		1, 0, 0,
		0, 0, 1;
	Eigen::Matrix3d firstRotationMatrix = svdEssential.matrixU()*matW*svdEssential.matrixV().transpose();
	Eigen::Matrix3d secondRotationMatrix = svdEssential.matrixU()*matW.transpose()*svdEssential.matrixV().transpose();
	double rotationDeterminant = firstRotationMatrix.determinant();
	if (rotationDeterminant < 0) {
		firstRotationMatrix *= -1.0;
		secondRotationMatrix *= -1.0;
	}
	Eigen::Matrix3d rotationMatrix;
	if (firstRotationMatrix(0, 0) > 0 && firstRotationMatrix(1, 1) > 0 && firstRotationMatrix(2, 2) > 0) {
		rotationMatrix = firstRotationMatrix;
	} else {
		rotationMatrix = secondRotationMatrix;
	}
    //cout<<"rotation: "<<rotationMatrix<<endl;
    cout<<"translation: "<<svdEssential.matrixU()<<endl;
	Matrix3d rotationHomographyMatrix_ = calibrationMatrix_*rotationMatrix*calibrationMatrix_.inverse();
	return rotationHomographyMatrix_;
}

void ReadHomographyMatrix()
{
    homographyMatrix = Matrix3d::Identity(3,3);

    if(strcmp(hm_name,"")!=0)
    {
         ifstream myfile(hm_name);
         for(int i=0;i<3;i++)
         {
             for(int j=0;j<3;j++)
             {
                 myfile>>homographyMatrix(i,j);
             }
         }
         myfile.close();
    }
    cout<<homographyMatrix<<endl;
}

void SaveRes(MatrixXf res)
{
    float curMax=0;
    for(int h=0;h<height;h++)
    {
        for(int w=0;w<width;w++)
        {
            if(res(h,w)>curMax)
            {
                curMax=res(h,w);
            }
        }
    }
    Mat myMat(height, width, CV_8UC1);
    for(int h=0;h<height;h++)
    {
        for(int w=0;w<width;w++)
        {
            myMat.at<uchar>(h,w)=255*pow(res(h,w)/curMax,0.5);
        }
    }
    imwrite("depth_out.png", myMat);
}

void ComputeSecondEpipole(double& secondEpipoleX, double& secondEpipoleY)
{
    Eigen::MatrixXd matFt = fm.transpose();
	Eigen::JacobiSVD<Eigen::MatrixXd> svdMatFt(matFt, Eigen::ComputeFullV);

	secondEpipoleX = svdMatFt.matrixV()(0, 2)/svdMatFt.matrixV()(2, 2);
	secondEpipoleY = svdMatFt.matrixV()(1, 2)/svdMatFt.matrixV()(2, 2);
}


MatrixXf EstimateSearchRange(Matrix3d hm)
{
    double secondEpipoleX, secondEpipoleY;
    ComputeSecondEpipole(secondEpipoleX, secondEpipoleY);
    MatrixXf res=MatrixXf::Zero(height,width);
    for(int h=0;h<height;h++)
    {
        for(int w=0;w<width;w++)
        {
            if(fx(h,w)==0 || fy(h,w)==0)
                continue;
           // if(mask(h,w)==1)
           //     continue;
            int x=w, y=h;
            Vector3d epipolarLine;
            computeSecondEpipolarLine(x,y,epipolarLine);

            Vector3d imagePoint;
            imagePoint << x,y,1;
            Vector3d rotatedPoint = hm * imagePoint;
            rotatedPoint /= rotatedPoint(2);

            double coefficientToEpipolarLine = -epipolarLine(0)*rotatedPoint(0)
			- epipolarLine(1)*rotatedPoint(1) - epipolarLine(2);
            double noDisparityPointX = rotatedPoint(0) + epipolarLine(0)*coefficientToEpipolarLine;
			double noDisparityPointY = rotatedPoint(1) + epipolarLine(1)*coefficientToEpipolarLine;

			double r1 = sqrt((noDisparityPointX - secondEpipoleX)*(noDisparityPointX - secondEpipoleX)
				+ (noDisparityPointY - secondEpipoleY)*(noDisparityPointY - secondEpipoleY));

            double epipolarDirectionX = -epipolarLine(1);
			double epipolarDirectionY = epipolarLine(0);

			double secondX=x+fx(h,w), secondY=y+fy(h,w);
			double a=-epipolarDirectionY/(secondEpipoleX*epipolarDirectionY-secondEpipoleY*epipolarDirectionX),
			b=epipolarDirectionX/(secondEpipoleX*epipolarDirectionY-secondEpipoleY*epipolarDirectionX),
			c=1;

            double dist1=sqrt((secondX-secondEpipoleX)*(secondX-secondEpipoleX)
                              +(secondY-secondEpipoleY)*(secondY-secondEpipoleY)),
                              dist2=fabs(a*secondX+b*secondY+c)/sqrt(a*a+b*b);
            double r2=sqrt(dist1*dist1-dist2*dist2);
            //cout<<x<<" "<<y<<" "<<(r2-r1)/r1<<endl;
            //res(h,w)=(r2-r1)/r1;
            res(h,w)=(r2-r1)/r1;
            //res(h,w)=dist2;
        }
    }
    return res;
}

void ComputeFirstEpipole(double& firstEpipoleX, double& firstEpipoleY)
{
	Eigen::MatrixXd matF = fm;
	Eigen::JacobiSVD<Eigen::MatrixXd> svdMatF(matF, Eigen::ComputeFullV);

	firstEpipoleX = svdMatF.matrixV()(0, 2)/svdMatF.matrixV()(2, 2);
	firstEpipoleY = svdMatF.matrixV()(1, 2)/svdMatF.matrixV()(2, 2);
}


void ComputeFirstEpipolarLine(int x, int y, double firstEpipoleX, double firstEpipoleY, double& a, double& b)
{
    b=(x-firstEpipoleX)/(y*firstEpipoleX-firstEpipoleY*x);
    a=(firstEpipoleY-y)/(y*firstEpipoleX-firstEpipoleY*x);
}

void ComputeProjectionOnEpipolarLine(double x0, double y0, double a, double b, double c, double& x1, double& y1)
{
    x1=(-a*b*y0+b*b*x0-a*c)/(a*a+b*b);
    y1=(a*a*y0-a*b*x0-b*c)/(a*a+b*b);
}

void TestEpipolar3()
{
    double firstEpipoleX, firstEpipoleY;
    ComputeFirstEpipole(firstEpipoleX, firstEpipoleY);
    double secondEpipoleX, secondEpipoleY;
    ComputeSecondEpipole(secondEpipoleX,secondEpipoleY);
    double fl=300;
    int yes=0, no=0;
    for(int h=0;h<height;h++)
    {
        for(int w=0;w<width;w++)
        {
            int x=w, y=h;
            Vector3f v1(x,y,fl), v2(firstEpipoleX,firstEpipoleY,fl);

            double secondX=x+fx(h,w), secondY=y+fy(h,w);
            double x3, y3;
            Vector3d epipolarLine;
            computeSecondEpipolarLine(x,y,epipolarLine);

             double a2=epipolarLine(0)/epipolarLine(2), b2=epipolarLine(1)/epipolarLine(2);

            ComputeProjectionOnEpipolarLine(secondX, secondY, a2, b2, x3, y3);
            Vector3f v3(x3,y3,fl), v4(secondEpipoleX, secondEpipoleY,fl);
            double angle1=acos(v1.dot(v2)/(v1.norm()*v2.norm())),
            angle2=acos(v3.dot(v4)/(v3.norm()*v4.norm()));
            if(angle1<angle2)
                yes++;
            else
                no++;
        }
    }
    cout<<yes<<" "<<no<<endl;
}

void TestEpipolar2(Matrix3d hm)
{
    double secondEpipoleX, secondEpipoleY;
    ComputeSecondEpipole(secondEpipoleX,secondEpipoleY);
    int yes=0, no=0;
    for(int h=0;h<height;h++)
    {
        for(int w=0;w<width;w++)
        {
            int x=w, y=h;

            Vector3d epipolarLine;
            computeSecondEpipolarLine(x,y,epipolarLine);

            Vector3d imagePoint;
            imagePoint << x,y,1;
            Vector3d rotatedPoint = hm * imagePoint;
            rotatedPoint /= rotatedPoint(2);

            double coefficientToEpipolarLine = -epipolarLine(0)*rotatedPoint(0)
			- epipolarLine(1)*rotatedPoint(1) - epipolarLine(2);
            double noDisparityPointX = rotatedPoint(0) + epipolarLine(0)*coefficientToEpipolarLine;
			double noDisparityPointY = rotatedPoint(1) + epipolarLine(1)*coefficientToEpipolarLine;

            double a2=epipolarLine(0)/epipolarLine(2), b2=epipolarLine(1)/epipolarLine(2);

            Vector2f v1;
            v1(0)=noDisparityPointX-secondEpipoleX;
            v1(1)=noDisparityPointY-secondEpipoleY;
            v1=v1/(v1(0)*v1(0)+v1(1)*v1(1));

            double secondX=x+fx(h,w), secondY=y+fy(h,w);
            double x3, y3;
            ComputeProjectionOnEpipolarLine(secondX, secondY, a2, b2, x3, y3);

            Vector2f v2;
            v2(0)=x3-noDisparityPointX;
            v2(1)=y3-noDisparityPointY;
            v2=v2/(v2(0)*v2(0)+v2(1)*v2(1));

            if(v1(0)*v2(0)+v1(1)*v2(1)>0)
                yes++;
            else
                no++;
        }
    }
    cout<<yes<<" "<<no<<endl;
}

// This function tests whether we can fix the search range of epipolar
void TestEpipolar()
{
    double firstEpipoleX, firstEpipoleY;
    ComputeFirstEpipole(firstEpipoleX, firstEpipoleY);
    double secondEpipoleX, secondEpipoleY;
    ComputeSecondEpipole(secondEpipoleX,secondEpipoleY);
    int yes=0, no=0;
    for(int h;h<height;h++)
    {
        for(int w=0;w<width;w++)
        {
            int x=w, y=h;
            double a,b;
            //the epipolar line is ax+by+1=0;
            ComputeFirstEpipolarLine(x,y,firstEpipoleX,firstEpipoleY,a,b);
            double centerX=width/2.0, centerY=height/2.0;
            double x1,y1;
            ComputeProjectionOnEpipolarLine(centerX, centerY, a, b, x1, y1);
            double distFromEpipole=sqrt((x1-x)*(x1-x)+(y1-y)*(y1-y));
            Vector3d epipolarLine;
            computeSecondEpipolarLine(x,y,epipolarLine);
            double a2=epipolarLine(0)/epipolarLine(2), b2=epipolarLine(1)/epipolarLine(2);
            double x2, y2;
            ComputeProjectionOnEpipolarLine(centerX, centerY, a2, b2, x2, y2);

            double epipolarDirectionX = -epipolarLine(1);
			double epipolarDirectionY = epipolarLine(0);

			double secondX=x+fx(h,w), secondY=y+fy(h,w);
			double a3=-epipolarDirectionY/(x2*epipolarDirectionY-y2*epipolarDirectionX),
			b3=epipolarDirectionX/(x2*epipolarDirectionY-y2*epipolarDirectionX),
			c3=1;

            double dist1=sqrt((secondX-x2)*(secondX-x2)
                              +(secondY-y2)*(secondY-y2)),
                              dist2=fabs(a3*secondX+b3*secondY+c3)/sqrt(a3*a3+b3*b3);
            double r2=sqrt(dist1*dist1-dist2*dist2);

            Vector2f v1;
            v1(0)=x-x1;
            v1(1)=y-y1;
            v1=v1/(v1(0)*v1(0)+v1(1)*v1(1));

            Vector2f v2;
            if(epipolarDirectionX*v1(0)+epipolarDirectionY*v1(1)>0)
            {
                v2(0)=epipolarDirectionX;
                v2(1)=epipolarDirectionY;
            }
            else
            {
                v2(0)=-epipolarDirectionX;
                v2(1)=-epipolarDirectionY;
            }

            Vector2f p2;
            p2(0)=x2+v2(0)*distFromEpipole;
            p2(1)=y2+v2(1)*distFromEpipole;

            double x3, y3;
            ComputeProjectionOnEpipolarLine(secondX, secondY, a2, b2, x3, y3);

            Vector2f v3;
            v3(0)=x3-p2(0);
            v3(1)=y3-p2(1);

            v3=v3/(v3(0)*v3(0)+v3(1)*v3(1));

            Vector2f v4;
            v4(0)=x3-secondEpipoleX;
            v4(1)=y3-secondEpipoleY;

            v4=v4/(v4(0)*v4(0)+v4(1)*v4(1));

            if(v3(0)*v2(0)+v3(1)*v2(1)>0)
                yes++;
            else
                no++;

            /*if(v4(0)*v3(0)+v4(1)*v3(1)>0)
                yes++;
            else
                no++;*/
        }
    }
    cout<<yes<<" "<<no<<endl;
}

int main(int argc, char** argv)
{
    ParseArguments(argc,argv);
    ReadData();
    Matrix3d calib=GetRandomCalibMatrix();
    Matrix3d hm=ComputeHomographyMatrix(calib,fm);
    //MatrixXf res=EstimateSearchRange(hm);
    //TestEpipolar2(hm);
    TestEpipolar3();
    //ofstream file("depth.txt");
    //file<<res<<endl;
    //SaveRes(res);
}

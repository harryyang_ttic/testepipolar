#include "FundamentalMatrix.h"
#include <fstream>
#include <sstream>
#include <cmath>
#include <float.h>

const double FUNDAMENTALMATRIX_DEFAULT_CONFIDENCE_LEVEL = 0.99;
const double FUNDAMENTALMATRIX_DEFAULT_INLIER_THRESHOLD = 1.0;
const int FUNDAMENTALMATRIX_DEFAULT_EPIPOLAR_SEARCH_DIRECTION_FLAG = 0;

inline int computeRequiredSamplingTotal(const int drawTotal, const double outlierRatio, const double confidenceLevel) {
    return static_cast<int>(log(1.0 - confidenceLevel)/log(1.0 - pow(1.0 - outlierRatio, drawTotal)) + 0.5);
}


FundamentalMatrix::FundamentalMatrix() : confidenceLevel_(FUNDAMENTALMATRIX_DEFAULT_CONFIDENCE_LEVEL),
                                         inlierThreshold_(FUNDAMENTALMATRIX_DEFAULT_INLIER_THRESHOLD),
                                         epipolarSearchDirectionFlag_(FUNDAMENTALMATRIX_DEFAULT_EPIPOLAR_SEARCH_DIRECTION_FLAG)
{
    matrixData_.setConstant(0.0);
}

void FundamentalMatrix::setRobustEstimatorParameters(const double confidenceLevel, const double inlierThreshold) {
    if (confidenceLevel <= 0 || confidenceLevel >= 1.0) {
        throw rev::Exception("FundamentalMatrix::setRobustEstimatorParameters", "confidence level is out of range");
    }
    confidenceLevel_ = confidenceLevel;
    
    if (inlierThreshold < 0) {
        throw rev::Exception("FundamentalMatrix::setRobustEstimatorParameters", "inlier threshold is negative");
    }
    inlierThreshold_ = inlierThreshold;
}

void FundamentalMatrix::estimateSelectionLMedS(const std::vector<MatchedPoint>& matchedPoints) {
    double pointDistanceThreshold = 30;
    
    std::vector<MatchedPoint> selectedPoints;
    selectMatchedPoint(matchedPoints, pointDistanceThreshold, selectedPoints);
        
    Eigen::Matrix3d estimatedFundamentalMatrix;
    std::vector<bool> estimatedInlierFlags;
    estimateLMedS(selectedPoints, estimatedFundamentalMatrix, estimatedInlierFlags);
        
    matrixData_ = estimatedFundamentalMatrix;
}

void FundamentalMatrix::estimateLMedS(const std::vector<MatchedPoint>& matchedPoints) {
    Eigen::Matrix3d matF;
    std::vector<bool> inlierFlags;
    estimateLMedS(matchedPoints, matF, inlierFlags);
    
    matrixData_ = matF;
}

void FundamentalMatrix::estimate8PointAlgorithm(const std::vector<MatchedPoint>& matchedPoints) {
    Eigen::Matrix3d matF;
    compute8PointAlgorithm(matchedPoints, matF);
    
    matrixData_ = matF;
}

void FundamentalMatrix::estimateEpipolarSearchDirection(const int imageWidth, const int imageHeight,
                                                        const std::vector<MatchedPoint>& matchedPoint)
{
    double epipoleX, epipoleY;
    computeSecondEpipole(epipoleX, epipoleY);

    Eigen::VectorXd rotationFlowParameter;
    computeRotationFlowParameter(imageWidth, imageHeight, rotationFlowParameter);
    
    int expansionCount = 0;
    int pointTotal = static_cast<int>(matchedPoint.size());
    for (int pointIndex = 0; pointIndex < pointTotal; ++pointIndex) {
        double firstX = matchedPoint[pointIndex].firstX();
        double firstY = matchedPoint[pointIndex].firstY();

        Eigen::Vector3d epipolarLine;
        computeSecondEpipolarLine(firstX, firstY, epipolarLine);
        
        double normalizedX = firstX - imageWidth/2.0;
        double normalizedY = firstY - imageHeight/2.0;
        double rotationFlowX = rotationFlowParameter(0)
                                - rotationFlowParameter(2)*normalizedY
                                + rotationFlowParameter(3)*normalizedX*normalizedX
                                + rotationFlowParameter(4)*normalizedX*normalizedY;
        double rotationFlowY = rotationFlowParameter(1)
                                + rotationFlowParameter(2)*normalizedX
                                + rotationFlowParameter(3)*normalizedX*normalizedY
                                + rotationFlowParameter(4)*normalizedY*normalizedY;
        
        double noDisparityCoefficient = -epipolarLine(0)*(firstX + rotationFlowX)
                                        - epipolarLine(1)*(firstY + rotationFlowY)
                                        - epipolarLine(2);
        double noDisparityX = firstX + rotationFlowX + epipolarLine(0)*noDisparityCoefficient;
        double noDisparityY = firstY + rotationFlowY + epipolarLine(1)*noDisparityCoefficient;
        
        double secondX = matchedPoint[pointIndex].secondX();
        double secondY = matchedPoint[pointIndex].secondY();
        
        double startDistance = (noDisparityX - epipoleX)*(noDisparityX - epipoleX) + (noDisparityY - epipoleY)*(noDisparityY - epipoleY);
        double endDistance = (secondX - epipoleX)*(secondX - epipoleX) + (secondY - epipoleY)*(secondY - epipoleY);
        
        if (endDistance >= startDistance) ++expansionCount;
        if (expansionCount > pointTotal/2) break;
    }

    if (expansionCount > pointTotal/2) epipolarSearchDirectionFlag_ = 0;
    else epipolarSearchDirectionFlag_ = 1;
}

void FundamentalMatrix::computeFirstEpipolarLine(const double secondX, const double secondY, Eigen::Vector3d& firstEpipolarLine) const {
    Eigen::Vector3d secondPoint;
    secondPoint << secondX, secondY, 1.0;
    
    firstEpipolarLine = matrixData_.transpose()*secondPoint;
    double normalizeFactor = sqrt(firstEpipolarLine(0)*firstEpipolarLine(0) + firstEpipolarLine(1)*firstEpipolarLine(1));
    if (normalizeFactor > 1e-6) normalizeFactor = 1.0/normalizeFactor;
    else normalizeFactor = 1.0;
    firstEpipolarLine *= normalizeFactor;
}

void FundamentalMatrix::computeSecondEpipolarLine(const double firstX, const double firstY, Eigen::Vector3d& secondEpipolarLine) const {
    Eigen::Vector3d firstPoint;
    firstPoint << firstX, firstY, 1.0;
    
    secondEpipolarLine = matrixData_*firstPoint;
    double normalizeFactor = sqrt(secondEpipolarLine(0)*secondEpipolarLine(0) + secondEpipolarLine(1)*secondEpipolarLine(1));
    if (normalizeFactor > 1e-6) normalizeFactor = 1.0/normalizeFactor;
    else normalizeFactor = 1.0;
    secondEpipolarLine *= normalizeFactor;
}

void FundamentalMatrix::computeFirstEpipole(double& firstEpipoleX, double& firstEpipoleY) const {
    Eigen::MatrixXd matF = matrixData_;
    Eigen::JacobiSVD<Eigen::MatrixXd> svdMatF(matF, Eigen::ComputeFullV);
    
    firstEpipoleX = svdMatF.matrixV()(0, 2)/svdMatF.matrixV()(2, 2);
    firstEpipoleY = svdMatF.matrixV()(1, 2)/svdMatF.matrixV()(2, 2);
}

void FundamentalMatrix::computeSecondEpipole(double& secondEpipoleX, double& secondEpipoleY) const {
    Eigen::MatrixXd matFt = matrixData_.transpose();
    Eigen::JacobiSVD<Eigen::MatrixXd> svdMatFt(matFt, Eigen::ComputeFullV);
    
    secondEpipoleX = svdMatFt.matrixV()(0, 2)/svdMatFt.matrixV()(2, 2);
    secondEpipoleY = svdMatFt.matrixV()(1, 2)/svdMatFt.matrixV()(2, 2);
}

void FundamentalMatrix::computeRotationFlowParameter(const int imageWidth, const int imageHeight,
                                                     Eigen::VectorXd& rotationFlowParameter) const
{
    std::vector< std::vector<double> > basePoints(8);
    for (int i = 0; i < 8; ++i) basePoints[i].resize(2);
    basePoints[0][0] = 100;  basePoints[0][1] = 50;
    basePoints[1][0] = imageWidth/2.0;  basePoints[1][1] = 50;
    basePoints[2][0] = imageWidth - 100;  basePoints[2][1] = 50;
    basePoints[3][0] = 100;  basePoints[3][1] = imageHeight/2.0;
    basePoints[4][0] = imageWidth - 100;  basePoints[4][1] = imageHeight/2.0;
    basePoints[5][0] = 100;  basePoints[5][1] = imageHeight - 50;
    basePoints[6][0] = imageWidth/2.0;  basePoints[6][1] = imageHeight - 50;
    basePoints[7][0] = imageWidth - 100;  basePoints[7][1] = imageHeight - 50;
    std::vector<Eigen::Vector3d> baseEpipolarLines(8);
    for (int i = 0;i < 8; ++i) {
        computeSecondEpipolarLine(basePoints[i][0], basePoints[i][1], baseEpipolarLines[i]);
    }
    
    Eigen::MatrixXd matCoefficient(8, 5);
    Eigen::VectorXd vecConstant(8);
    for (int i = 0; i < 8; ++i) {
        double normalizedX = basePoints[i][0] - imageWidth/2.0;
        double normalizedY = basePoints[i][1] - imageHeight/2.0;
        
        matCoefficient(i, 0) = baseEpipolarLines[i](0);
        matCoefficient(i, 1) = baseEpipolarLines[i](1);
        matCoefficient(i, 2) = -baseEpipolarLines[i](0)*normalizedY + baseEpipolarLines[i](1)*normalizedX;
        matCoefficient(i, 3) = baseEpipolarLines[i](0)*normalizedX*normalizedX + baseEpipolarLines[i](1)*normalizedX*normalizedY;
        matCoefficient(i, 4) = baseEpipolarLines[i](0)*normalizedX*normalizedY + baseEpipolarLines[i](1)*normalizedY*normalizedY;
        
        vecConstant(i) = -(baseEpipolarLines[i](0)*basePoints[i][0] + baseEpipolarLines[i](1)*basePoints[i][1] + baseEpipolarLines[i](2));
    }
    rotationFlowParameter = matCoefficient.colPivHouseholderQr().solve(vecConstant);
}

void FundamentalMatrix::readFundamentalMatrixFile(const std::string filename) {
    std::ifstream fundamentalFileStream(filename.c_str(), std::ios_base::in | std::ios_base::binary);
    if (fundamentalFileStream.fail()) {
        std::stringstream errorMessage;
        errorMessage << "can't open file (" << filename << ")";
        throw rev::Exception("FundamentalMatrix::readFundamentalMatrixFile", errorMessage.str());
    }
    double elements[9];
    int directionFlag;
    fundamentalFileStream.read(reinterpret_cast<char*>(elements), 9*sizeof(double));
    fundamentalFileStream.read(reinterpret_cast<char*>(&directionFlag), sizeof(int));
    fundamentalFileStream.close();
    
    matrixData_ << elements[0], elements[1], elements[2],
                   elements[3], elements[4], elements[5],
                   elements[6], elements[7], elements[8];
    epipolarSearchDirectionFlag_ = directionFlag;
}

void FundamentalMatrix::writeFundamentalMatrixFile(const std::string filename) const {
    std::ofstream fundamentalFileStream(filename.c_str(), std::ios_base::out | std::ios_base::binary);
    if (fundamentalFileStream.fail()) {
        std::stringstream errorMessage;
        errorMessage << "can't open file (" << filename << ")";
        throw rev::Exception("FundamentalMatrix::writeFundamentalMatrixFile", errorMessage.str());
    }
    double elements[9];
    elements[0] = matrixData_(0, 0);
    elements[1] = matrixData_(0, 1);
    elements[2] = matrixData_(0, 2);
    elements[3] = matrixData_(1, 0);
    elements[4] = matrixData_(1, 1);
    elements[5] = matrixData_(1, 2);
    elements[6] = matrixData_(2, 0);
    elements[7] = matrixData_(2, 1);
    elements[8] = matrixData_(2, 2);
    fundamentalFileStream.write(reinterpret_cast<char*>(elements), 9*sizeof(double));
    int directionFlag = epipolarSearchDirectionFlag_;
    fundamentalFileStream.write(reinterpret_cast<char*>(&directionFlag), sizeof(int));
    fundamentalFileStream.close();
}


void FundamentalMatrix::selectMatchedPoint(const std::vector<MatchedPoint>& matchedPoints,
                                           const double pointDistanceThreshold,
                                           std::vector<MatchedPoint>& selectedPoints) const
{
    const double pointDistanceThresholdSquare = pointDistanceThreshold*pointDistanceThreshold;
    
    selectedPoints.clear();
    
    int pointTotal = static_cast<int>(matchedPoints.size());
    for (int pointIndex = 0; pointIndex < pointTotal; ++pointIndex) {
        double pointX = matchedPoints[pointIndex].firstX();
        double pointY = matchedPoints[pointIndex].firstY();
        
        bool addFlag = true;
        for (int selectedIndex = 0; selectedIndex < static_cast<int>(selectedPoints.size()); ++selectedIndex) {
            double selectedPointX = selectedPoints[selectedIndex].firstX();
            double selectedPointY = selectedPoints[selectedIndex].firstY();
            
            double pointDistance = (pointX - selectedPointX)*(pointX - selectedPointX)
                                    + (pointY - selectedPointY)*(pointY - selectedPointY);
            
            if (pointDistance < pointDistanceThresholdSquare) {
                addFlag = false;
                break;
            }
        }
        if (addFlag) {
            selectedPoints.push_back(matchedPoints[pointIndex]);
        }
    }
}

void FundamentalMatrix::extractInlierPoint(const std::vector<MatchedPoint>& matchedPoints,
                                           const std::vector<bool>& inlierFlags,
                                           std::vector<MatchedPoint>& inlierMatchedPoints) const
{
    inlierMatchedPoints.clear();
    int pointTotal = static_cast<int>(matchedPoints.size());
    for (int pointIndex = 0; pointIndex < pointTotal; ++pointIndex) {
        if (inlierFlags[pointIndex]) {
            inlierMatchedPoints.push_back(matchedPoints[pointIndex]);
        }
    }
}

void FundamentalMatrix::estimateLMedS(const std::vector<MatchedPoint>& matchedPoints,
                                      Eigen::Matrix3d& fundamentalMatrix,
                                      std::vector<bool>& inlierFlags)
{
    const int drawTotal = 7;
    const double initialOutlierRatio = 0.45;
    
    rev::RandomNumberGenerator randomGenerator;
    
    int pointTotal = static_cast<int>(matchedPoints.size());
    int samplingTotal = computeRequiredSamplingTotal(drawTotal, initialOutlierRatio, confidenceLevel_);
    
    double minMedianSquareError = DBL_MAX;
    Eigen::Matrix3d bestFundamentalMatrix;
    std::vector<double> bestEpipolarSquareErrors;
    
    int samplingCount = 0;
    while (samplingCount < samplingTotal) {
        std::vector<MatchedPoint> sampledPoints;
        randomDrawMatchedPoints(matchedPoints, drawTotal, randomGenerator, sampledPoints);
        
        std::vector<Eigen::Matrix3d> fundamentalMatrixCandidates;
        compute7PointAlgorithm(sampledPoints, fundamentalMatrixCandidates);
        int candidateTotal = static_cast<int>(fundamentalMatrixCandidates.size());
        for (int candidateIndex = 0; candidateIndex < candidateTotal; ++candidateIndex) {
            std::vector<double> epipolarSquareErrors;
            computeEpipolarSquareErrors(matchedPoints, fundamentalMatrixCandidates[candidateIndex], epipolarSquareErrors);
            
            double candidateMedianSquareError;
            std::vector<double> sortedErrors = epipolarSquareErrors;
            std::sort(sortedErrors.begin(), sortedErrors.end());
            if (pointTotal %2 != 0) candidateMedianSquareError = sortedErrors[pointTotal/2];
            else candidateMedianSquareError = (sortedErrors[pointTotal/2 - 1] + sortedErrors[pointTotal/2])*0.5;
            
            if (candidateMedianSquareError < minMedianSquareError) {
                minMedianSquareError = candidateMedianSquareError;
                bestFundamentalMatrix = fundamentalMatrixCandidates[candidateIndex];
                bestEpipolarSquareErrors = epipolarSquareErrors;
            }
        }
        
        ++samplingCount;
    }
    
    fundamentalMatrix = bestFundamentalMatrix;
    
    inlierThreshold_ = 2.5*1.4826*(1.0 + 5.0/(pointTotal - drawTotal))*sqrt(minMedianSquareError);
    if (inlierThreshold_ < 0.001) inlierThreshold_ = 0.001;
    countInliers(bestEpipolarSquareErrors, inlierFlags);
}

void FundamentalMatrix::compute8PointAlgorithm(const std::vector<MatchedPoint>& matchedPoints,
                                               Eigen::Matrix3d& fundamentalMatrix) const
{
    int pointTotal = static_cast<int>(matchedPoints.size());
    if (pointTotal < 8) {
        throw rev::Exception("FundamentalMatrix::compute8PointAlgorithm", "the number of points is less than 8");
    }
    
    // Compute centers
    double firstCenterX = 0.0, firstCenterY = 0.0;
    double secondCenterX = 0.0, secondCenterY = 0.0;
    for (int pointIndex = 0; pointIndex < pointTotal; ++pointIndex) {
        firstCenterX += matchedPoints[pointIndex].firstX();
        firstCenterY += matchedPoints[pointIndex].firstY();
        secondCenterX += matchedPoints[pointIndex].secondX();
        secondCenterY += matchedPoints[pointIndex].secondY();
    }
    firstCenterX /= pointTotal;
    firstCenterY /= pointTotal;
    secondCenterX /= pointTotal;
    secondCenterY /= pointTotal;
    // Compute scales
    double firstScale = 0.0;
    double secondScale = 0.0;
    for (int pointIndex = 0; pointIndex < pointTotal; ++pointIndex) {
        double x = matchedPoints[pointIndex].firstX() - firstCenterX;
        double y = matchedPoints[pointIndex].firstY() - firstCenterY;
        firstScale += sqrt(x*x + y*y);
        
        x = matchedPoints[pointIndex].secondX() - secondCenterX;
        y = matchedPoints[pointIndex].secondY() - secondCenterY;
        secondScale += sqrt(x*x + y*y);
    }
    firstScale /= pointTotal;
    secondScale /= pointTotal;
    if (firstScale < DBL_EPSILON || secondScale < DBL_EPSILON) return;
    firstScale = sqrt(2.0)/firstScale;
    secondScale = sqrt(2.0)/secondScale;
    
    Eigen::MatrixXd matA(9, 9);
    matA.setConstant(0.0);
    for (int pointIndex = 0; pointIndex < pointTotal; ++pointIndex) {
        double normalizedFirstX = (matchedPoints[pointIndex].firstX() - firstCenterX)*firstScale;
        double normalizedFirstY = (matchedPoints[pointIndex].firstY() - firstCenterY)*firstScale;
        double normalizedSecondX = (matchedPoints[pointIndex].secondX() - secondCenterX)*secondScale;
        double normalizedSecondY = (matchedPoints[pointIndex].secondY() - secondCenterY)*secondScale;
        
        std::vector<double> elements(9);
        elements[0] = normalizedSecondX*normalizedFirstX;
        elements[1] = normalizedSecondX*normalizedFirstY;
        elements[2] = normalizedSecondX;
        elements[3] = normalizedSecondY*normalizedFirstX;
        elements[4] = normalizedSecondY*normalizedFirstY;
        elements[5] = normalizedSecondY;
        elements[6] = normalizedFirstX;
        elements[7] = normalizedFirstY;
        elements[8] = 1.0;
        
        for (int rowIndex = 0; rowIndex < 9; ++rowIndex) {
            for (int columnIndex = 0; columnIndex < 9; ++columnIndex) {
                matA(rowIndex, columnIndex) += elements[rowIndex]*elements[columnIndex];
            }
        }
    }
    
    Eigen::SelfAdjointEigenSolver<Eigen::MatrixXd> eigenMatA(matA);
    if (eigenMatA.info() != Eigen::Success) return;
    for (int i = 0; i < 9; ++i) {
        if (fabs(eigenMatA.eigenvalues()(i)) < DBL_EPSILON) return;
    }
    
    Eigen::MatrixXd matF(3, 3);
    matF << eigenMatA.eigenvectors()(0, 0), eigenMatA.eigenvectors()(1, 0), eigenMatA.eigenvectors()(2, 0),
    eigenMatA.eigenvectors()(3, 0), eigenMatA.eigenvectors()(4, 0), eigenMatA.eigenvectors()(5, 0),
    eigenMatA.eigenvectors()(6, 0), eigenMatA.eigenvectors()(7, 0), eigenMatA.eigenvectors()(8, 0);
    
    Eigen::JacobiSVD<Eigen::MatrixXd> svdMatF(matF, Eigen::ComputeFullU | Eigen::ComputeFullV);
    Eigen::MatrixXd matW(3, 3);
    matW.setConstant(0.0);
    matW(0, 0) = svdMatF.singularValues()(0);
    matW(1, 1) = svdMatF.singularValues()(1);
    
    matF = svdMatF.matrixU()*matW*svdMatF.matrixV().transpose();
    
    Eigen::Matrix3d firstTransformMatrix;
    firstTransformMatrix << firstScale, 0.0, -firstScale*firstCenterX,
    0.0, firstScale, -firstScale*firstCenterY,
    0.0, 0.0, 1.0;
    Eigen::Matrix3d secondTransformMatrix;
    secondTransformMatrix << secondScale, 0.0, -secondScale*secondCenterX,
    0.0, secondScale, -secondScale*secondCenterY,
    0.0, 0.0, 1.0;
    
    fundamentalMatrix = secondTransformMatrix.transpose()*matF*firstTransformMatrix;
    fundamentalMatrix /= fundamentalMatrix(2, 2);
}

void FundamentalMatrix::randomDrawMatchedPoints(const std::vector<MatchedPoint>& matchedPoints,
                                                const int drawTotal,
                                                rev::RandomNumberGenerator& randomGenerator,
                                                std::vector<MatchedPoint>& sampledPoints) const
{
    int pointTotal = static_cast<int>(matchedPoints.size());
    
    std::vector<int> sampledIndices(drawTotal);
    for (int i = 0; i < drawTotal; ++i) {
        int randomIndex;
        bool drawFlag = true;
        while (drawFlag) {
            randomIndex = randomGenerator.randomInteger() % pointTotal;
            drawFlag = false;
            for (int j = 0; j < i; ++j) {
                if (randomIndex == sampledIndices[j]) {
                    drawFlag = true;
                    break;
                }
            }
        }
        sampledIndices[i] = randomIndex;
    }
    
    sampledPoints.resize(drawTotal);
    for (int i = 0; i < drawTotal; ++i) {
        sampledPoints[i] = matchedPoints[sampledIndices[i]];
    }
}

void FundamentalMatrix::compute7PointAlgorithm(const std::vector<MatchedPoint>& matchedPoints,
                                               std::vector<Eigen::Matrix3d>& fundamentalMatrices) const
{
    int pointTotal = static_cast<int>(matchedPoints.size());
    if (pointTotal < 7) {
        throw rev::Exception("FundamentalMatrix::compute7PointAlgorithm", "the number of points is less than 7");
    }
    
    Eigen::MatrixXd matA(7, 9);
    for (int i = 0; i < 7; ++i) {
        double firstX = matchedPoints[i].firstX();
        double firstY = matchedPoints[i].firstY();
        double secondX = matchedPoints[i].secondX();
        double secondY = matchedPoints[i].secondY();
        
        matA(i, 0) = secondX*firstX;
        matA(i, 1) = secondX*firstY;
        matA(i, 2) = secondX;
        matA(i, 3) = secondY*firstX;
        matA(i, 4) = secondY*firstY;
        matA(i, 5) = secondY;
        matA(i, 6) = firstX;
        matA(i, 7) = firstY;
        matA(i, 8) = 1.0;
    }
    
    Eigen::JacobiSVD<Eigen::MatrixXd> svdMatA(matA, Eigen::ComputeFullV);
    
    std::vector<double> f1(9), f2(9);
    for (int i = 0; i < 9; ++i) {
        f2[i] = svdMatA.matrixV()(i, 8);
        f1[i] = svdMatA.matrixV()(i, 7) - f2[i];
    }
    
    std::vector<double> coefficients(4);
    double t0 = f2[4]*f2[8] - f2[5]*f2[7];
    double t1 = f2[3]*f2[8] - f2[5]*f2[6];
    double t2 = f2[3]*f2[7] - f2[4]*f2[6];
    coefficients[3] = f2[0]*t0 - f2[1]*t1 + f2[2]*t2;
    coefficients[2] = f1[0]*t0 - f1[1]*t1 + f1[2]*t2
                      - f1[3]*(f2[1]*f2[8] - f2[2]*f2[7])
                      + f1[4]*(f2[0]*f2[8] - f2[2]*f2[6])
                      - f1[5]*(f2[0]*f2[7] - f2[1]*f2[6])
                      + f1[6]*(f2[1]*f2[5] - f2[2]*f2[4])
                      - f1[7]*(f2[0]*f2[5] - f2[2]*f2[3])
                      + f1[8]*(f2[0]*f2[4] - f2[1]*f2[3]);
    t0 = f1[4]*f1[8] - f1[5]*f1[7];
    t1 = f1[3]*f1[8] - f1[5]*f1[6];
    t2 = f1[3]*f1[7] - f1[4]*f1[6];
    coefficients[1] = f2[0]*t0 - f2[1]*t1 + f2[2]*t2
                      - f2[3]*(f1[1]*f1[8] - f1[2]*f1[7])
                      + f2[4]*(f1[0]*f1[8] - f1[2]*f1[6])
                      - f2[5]*(f1[0]*f1[7] - f1[1]*f1[6])
                      + f2[6]*(f1[1]*f1[5] - f1[2]*f1[4])
                      - f2[7]*(f1[0]*f1[5] - f1[2]*f1[3])
                      + f2[8]*(f1[0]*f1[4] - f1[1]*f1[3]);
    coefficients[0] = f1[0]*t0 - f1[1]*t1 + f1[2]*t2;
    
    std::vector<double> roots;
    solveCubicEquation(coefficients, roots);
    int rootTotal = static_cast<int>(roots.size());
    
    fundamentalMatrices.clear();
    if (rootTotal < 1) return;
    
    fundamentalMatrices.resize(rootTotal);
    for (int rootIndex = 0; rootIndex < rootTotal; ++rootIndex) {
        double lambda = roots[rootIndex];
        double s = f1[8]*roots[rootIndex] + f2[8];
        
        // Normalize
        double mu = 1.0;
        if (fabs(s) > DBL_EPSILON) {
            mu = 1.0/s;
            lambda *= mu;
            fundamentalMatrices[rootIndex](2, 2) = 1.0;
        } else {
            fundamentalMatrices[rootIndex](2, 2) = 0.0;
        }
        
        fundamentalMatrices[rootIndex](0, 0) = f1[0]*lambda + f2[0]*mu;
        fundamentalMatrices[rootIndex](0, 1) = f1[1]*lambda + f2[1]*mu;
        fundamentalMatrices[rootIndex](0, 2) = f1[2]*lambda + f2[2]*mu;
        fundamentalMatrices[rootIndex](1, 0) = f1[3]*lambda + f2[3]*mu;
        fundamentalMatrices[rootIndex](1, 1) = f1[4]*lambda + f2[4]*mu;
        fundamentalMatrices[rootIndex](1, 2) = f1[5]*lambda + f2[5]*mu;
        fundamentalMatrices[rootIndex](2, 0) = f1[6]*lambda + f2[6]*mu;
        fundamentalMatrices[rootIndex](2, 1) = f1[7]*lambda + f2[7]*mu;
    }
}

void FundamentalMatrix::computeEpipolarSquareErrors(const std::vector<MatchedPoint>& matchedPoints,
                                                    const Eigen::Matrix3d& estimatedFundamentalMatrix,
                                                    std::vector<double>& epipolarSquareErrors) const
{
    int pointTotal = static_cast<int>(matchedPoints.size());
    epipolarSquareErrors.resize(pointTotal);
    for (int pointIndex = 0; pointIndex < pointTotal; ++pointIndex) {
        Eigen::Vector3d firstPoint;
        firstPoint << matchedPoints[pointIndex].firstX(), matchedPoints[pointIndex].firstY(), 1.0;
        Eigen::Vector3d secondPoint;
        secondPoint << matchedPoints[pointIndex].secondX(), matchedPoints[pointIndex].secondY(), 1.0;
        
        Eigen::Vector3d firstEpipolarLine = estimatedFundamentalMatrix.transpose()*secondPoint;
        Eigen::Vector3d secondEpipolarLine = estimatedFundamentalMatrix*firstPoint;
        
        double firstDotProduct = firstEpipolarLine.dot(firstPoint);
        double firstEpipolarSquareError = firstDotProduct*firstDotProduct
                                            / (firstEpipolarLine(0)*firstEpipolarLine(0) + firstEpipolarLine(1)*firstEpipolarLine(1));
        double secondDotProduct = secondEpipolarLine.dot(secondPoint);
        double secondEpipolarSquareError = secondDotProduct*secondDotProduct
                                            / (secondEpipolarLine(0)*secondEpipolarLine(0) + secondEpipolarLine(1)*secondEpipolarLine(1));
        
        if (firstEpipolarSquareError > secondEpipolarSquareError) epipolarSquareErrors[pointIndex] = firstEpipolarSquareError;
        else epipolarSquareErrors[pointIndex] = secondEpipolarSquareError;
    }
}

int FundamentalMatrix::countInliers(const std::vector<double>& epipolarSquareErrors,
                                    std::vector<bool>& inlierFlags) const
{
    double inlierThresholdSquare = inlierThreshold_*inlierThreshold_;
    
    int pointTotal = static_cast<int>(epipolarSquareErrors.size());
    inlierFlags.resize(pointTotal);
    int inlierCount = 0;
    for (int pointIndex = 0; pointIndex < pointTotal; ++pointIndex) {
        if (epipolarSquareErrors[pointIndex] < inlierThresholdSquare) {
            inlierFlags[pointIndex] = true;
            ++inlierCount;
        } else {
            inlierFlags[pointIndex] = false;
        }
    }
    
    return inlierCount;
}


void FundamentalMatrix::solveCubicEquation(const std::vector<double>& coefficients,
                                           std::vector<double>& roots) const
{
    int n = 0;
    double x0 = 0.0, x1 = 0.0, x2 = 0.0;
    
    double a0 = coefficients[0];
    double a1 = coefficients[1];
    double a2 = coefficients[2];
    double a3 = coefficients[3];
    
    if (a0 == 0) {
        if (a1 == 0) {
            if (a2 == 0) {
                if (a3 == 0) n = -1;
                else n = 0;
            } else {
                // Linear equation
                n = 1;
                x0 = -a3/a2;
            }
        } else {
            // Quadratic equation
            double d = a2*a2 - 4*a1*a3;
            if (d >= 0) {
                d = sqrt(d);
                double q1 = (-a2 + d)*0.5;
                double q2 = (a2 + d)*(-0.5);
                if (fabs(q1) > fabs(q2)) {
                    x0 = q1/a1;
                    x1 = a3/q1;
                } else {
                    x0 = q2/a1;
                    x1 = a3/q2;
                }
                if (d > 0) n = 2;
                else n = 1;
            }
        }
    } else {
        a0 = 1.0/a0;
        a1 *= a0;
        a2 *= a0;
        a3 *= a0;
        
        double Q = (a1*a1 - 3*a2)/9.0;
        double R = (2.0*a1*a1*a1 - 9.0*a1*a2 + 27.0*a3)/54.0;
        double Qcubed = Q*Q*Q;
        double d = Qcubed - R*R;
        
        if (d >= 0) {
            double theta = acos(R/sqrt(Qcubed));
            double sqrtQ = sqrt(Q);
            double t0 = -2*sqrtQ;
            double t1 = theta/3.0;
            double t2 = a1/3.0;
            
            x0 = t0*cos(t1) - t2;
            x1 = t0*cos(t1 + 2.0*rev::REV_PI/3.0) - t2;
            x2 = t0*cos(t1 + 4.0*rev::REV_PI/3.0) - t2;
            n = 3;
        } else {
            d = sqrt(-d);
            double e = pow(d + fabs(R), 1.0/3.0);
            if (R > 0) e *= -1.0;
            x0 = (e + Q/e) - a1/3.0;
            n = 1;
        }
    }
    
    roots.clear();
    if (n > 0) {
        roots.resize(n);
        roots[0] = x0;
        if (n > 1) roots[1] = x1;
        if (n > 2) roots[2] = x2;
    }
}
